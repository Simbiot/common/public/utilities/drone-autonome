#ifndef _V4L2_OBJ
#define _V4L2_OBJ

#include <stdio.h>
#include <string.h>

#define ISP_MODE_PREVIEW	0x8000
#define ISP_MODE_VIDEO		0x4000
#define CLEAR(x) memset(&(x), 0, sizeof(x))

#define FLIP_NONE 0x00
#define FLIP_VERTICAL 0x01
#define FLIP_HORIZONTAL 0x02

typedef enum {
	IO_METHOD_READ,
	IO_METHOD_MMAP,
	IO_METHOD_USERPTR,
} io_method;


typedef enum {
	CAMERA_OV8858 = 0,
	CAMERA_OV7251
} CameraType;

typedef int (* callback_frame_t)(const void*, int) ;

class CV4l2Obj {

private:
	char *m_devName;
	io_method m_io;
	struct Buffer {
		void *start;
		size_t length;
	};
	struct Buffer *m_Buffers;
	unsigned int m_nBuffers;
protected:
	int m_width;
	int m_padded_width;
	int m_height;
	int m_format;
	int m_sizeimage;
	int m_cameraId, m_ispMode;
	callback_frame_t m_callback;
public:
	CV4l2Obj(CameraType camera, callback_frame_t callback);
	~CV4l2Obj();
	int stop_capturing(void);
	int start_capturing(void);
	int uninit_device(void);
	int init_device(void);
	int init_buffers(void);
	int open_device(void);
	int close_device(void);
	int mainloop(unsigned int count);
	int xioctl(int request, void *arg);

	int flip(int flip_type);
	int set_fps(int fps);
	int set_exposure(unsigned short time, unsigned char gain, unsigned short digital_gain);

	int m_fd;
private:
	int init_read(unsigned int buffer_size);
	int init_mmap(void);
	int init_userp(unsigned int buffer_size);
protected:
	int read_frame(void);
	int process_image(const void *p, int size);

	void errno_exit(const char *s);
};
#endif
