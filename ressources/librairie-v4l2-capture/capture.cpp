#include <linux/videodev2.h>
#include "v4l2_obj.h"

/* ISP Specfic parameter */
static int cam_input = 1; /* Camera ID selection */
static int main_width = 640;
static int main_height = 480;
static int main_format = V4L2_PIX_FMT_YUV420;
static int num_frame = 1;
static int test_pattern = 0;
static int fps = 30;

static void process_basic_preview();

int mon_callback(const void * buffer, int size)
{
	printf("Reçu une image de taille %d !\n", size);
	return 0;
}

int main(int argc, char **argv)
{
	CV4l2Obj camera(CAMERA_OV7251, mon_callback);
	int loop = num_frame;

	camera.flip(FLIP_VERTICAL);
	/* start capture */
	camera.start_capturing();

	camera.mainloop(loop);
	return 0;
}
