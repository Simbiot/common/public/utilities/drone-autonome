
#include <signal.h>
#include <fstream>
#include <sstream>
#include <regex>

#include <ros/package.h>
#include <mavros_msgs/PositionTarget.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/State.h>
#include <mavros_msgs/ParamGet.h>
#include <mavros_msgs/ParamSet.h>
#include <eigen_conversions/eigen_msg.h>

#include <navigation/commande_drone.hpp>

sig_atomic_t volatile interrupt_flag = false;

using namespace mavros_msgs;

void customSigHandler(int signal)
{
  ROS_INFO("User interrupt");
  interrupt_flag = true;
}

std::string describeSetPoint(const PositionTarget& sp)
{
  const static double props[] = {sp.position.x, sp.position.y, sp.position.z,
                                sp.velocity.x, sp.velocity.y, sp.velocity.z,
                                sp.acceleration_or_force.x, sp.acceleration_or_force.y, sp.acceleration_or_force.z,
                                1, //FORCE IN AF
                                sp.yaw, sp.yaw_rate};
  const static std::string labels[] = { "pos.x", "pos.y", "pos.z",
                                  "vel.x", "vel.y", "vel.z",
                                  "acc.x", "acc.y", "acc.z",
                                  "accel",
                                  "yaw", "yaw_rate"};

  std::stringstream stream;
  stream << "Coordinate frame id : " << (int) sp.coordinate_frame << std::endl;
  stream << "Type mask = 0x" << std::hex << sp.type_mask;
  for (int i = 0; i < sizeof(props) / sizeof(*props); i++)
  {
    if (!(1<<i & sp.type_mask))
      stream << "\n" << labels[i] << " : " << std::to_string(props[i]);
  }

  return stream.str();
}

void resolveHome(std::string& path)
{
  if (! path.size()) return;

  int pos = 0;

  std::string home = ros::package::getPath("navigation"); //Devrait être un paramètre pour etre utilisé en dehors du package navigation
  std::string placeholder = "PKG_HOME";

  while ((pos = path.find(placeholder, pos)) != std::string::npos)
    path.replace(pos, placeholder.length(), home);
}


CommandeDrone::CommandeDrone(const ros::NodeHandle& handle, bool sigintHandler) :
  spinner(2)
{
  enableDegugLog(false);
  shouldExit = false;

  this->nodeHandle = handle;

  //Using PositionTarget to send position, velocity and force setpoints at once
  //http://docs.ros.org/api/mavros_msgs/html/msg/PositionTarget.html
  setPointPub = nodeHandle.advertise<PositionTarget>("mavros/setpoint_raw/local", 5);

  visionIsPositionProvider = false;
  visionPosePub = nodeHandle.advertise<geometry_msgs::PoseStamped>("mavros/vision_pose/pose", 5);
  mocapPosePub = nodeHandle.advertise<geometry_msgs::PoseStamped>("mavros/mocap/pose", 5);

  armSrv = nodeHandle.serviceClient<CommandBool>("mavros/cmd/arming");
  setModeSrv = nodeHandle.serviceClient<SetMode>("mavros/set_mode");

  getParamSrv = nodeHandle.serviceClient<ParamGet>("mavros/param/get");
  setParamSrv = nodeHandle.serviceClient<ParamSet>("mavros/param/set");

  //Add battery, position and speed subscribers
  batterySub = nodeHandle.subscribe<sensor_msgs::BatteryState>("mavros/battery", 1, &CommandeDrone::batteryCb, this);
  stateSub = nodeHandle.subscribe<State>("mavros/state", 1, &CommandeDrone::stateCb, this);
  posSub = nodeHandle.subscribe<geometry_msgs::PoseStamped>("mavros/local_position/pose", 1, &CommandeDrone::posCb, this);
  speedSub = nodeHandle.subscribe<geometry_msgs::TwistStamped>("mavros/local_position/velocity_body", 1, &CommandeDrone::speedCb, this);
  rawIMUSub = nodeHandle.subscribe<sensor_msgs::Imu>("mavros/imu/data_raw", 1, &CommandeDrone::imuCb, this);


  if (sigintHandler)
    signal(SIGINT, customSigHandler);


  positionSetPoint = speedSetPoint = accelSetPoint = (Coords){0, 0, 0};
  yawSetPoint = yawRate = 0;
  setInfoSent(false, false, false, false);

  posCbCalls = 0;
  estimationUseMOCAP = false;
  posCalib = speedCalib = accelCalib = (Coords){0, 0, 0};

  if (!checkMAVROSConnection())
  {
    ROS_ERROR("MAVROS node is not launched or not publishing !");
    shouldExit = true;
    return;
  }


  currentState.started = currentState.connected = false;
  currentState.landed = true;

  //Ici ou dans init() ?
  spinner.start();
}

CommandeDrone::~CommandeDrone()
{
  //Si on a pas deja fait appel a exit
  logDebug("Destroying communication object");
  if (getState().started) exit();
  //Doit on vraiment faire ça ici ?
  //ros::shutdown();
}

//Passe en marm(ode offboard et lance le début du controle du drone
bool CommandeDrone::init(const std::string& calibrationFile, const std::string& parameterFile)
{

  if (!checkMAVROSConnection())
    return false;

  if (!arm())
    return false;

  if (!setMode(PX4_MODE_OFFBOARD))
    return false;


  if (! calibrationFile.empty())
  {
    if (!loadCalibration(calibrationFile))
      return false;
  }


  if (! parameterFile.empty())
  {
    if (!applyParametersFile(parameterFile))
      return false;
  }

  doPositionCalibration();

  shouldExit = false;
  communicationThread = std::thread(&CommandeDrone::runCommunicationThread, this);

  logDebug("Started communication thread");

  stateLock.lock();
  currentState.started = true;
  stateLock.unlock();

  return true;
}
//Repasse en mode manuel et fais atterir le drone/coupe les moteurs (si possible)
bool CommandeDrone::exit()
{
  if (!setMode(PX4_MODE_MANUAL))
    return false;

  arm(false);

  shouldExit = true;
  if (communicationThread.joinable()) communicationThread.join();

  logDebug("End of communication");

  if (!saveCalibration(DEFAULT_CALIBRATION_FILE))
  {
    ROS_WARN("Could not save calibration data to %s", DEFAULT_CALIBRATION_FILE);
    return false;
  }

  return true;
}

void CommandeDrone::doPositionCalibration()
{
  if (shouldExit) return;

  ROS_INFO("Calibrating position...");
  while (!posCbCalls && ros::ok() && !interrupt_flag){
    ros::Duration(.1).sleep();
  }
  if (ros::ok() && !interrupt_flag) {
    posCalib = getState().position;
    ROS_INFO("Done !");
    ROS_INFO("Origine : {x: %.2f, y: %.2f, z: %.2f}", posCalib.x, posCalib.y, posCalib.z);
   }
}



void CommandeDrone::setAccelerationTrimX(float xTrimInc) { accelCalib.x += xTrimInc; }
void CommandeDrone::setAccelerationTrimY(float yTrimInc) { accelCalib.y += yTrimInc; }
void CommandeDrone::setAccelerationTrimZ(float zTrimInc) { accelCalib.z += zTrimInc; }

void CommandeDrone::setSpeedTrimX(float xTrimInc) { speedCalib.x += xTrimInc; }
void CommandeDrone::setSpeedTrimY(float yTrimInc) { speedCalib.y += yTrimInc; }
void CommandeDrone::setSpeedTrimZ(float zTrimInc) { speedCalib.z += zTrimInc; }

//TODO: create parent dirs if they don't exist
bool CommandeDrone::saveCalibration(const std::string& file)
{
  Coords pos = posCalib, speed = speedCalib, accel = accelCalib;

  std::string absFile = file;
  resolveHome(absFile);

  std::ofstream fichier(absFile);
  if (!fichier)
    return false;

  fichier.exceptions( std::ifstream::failbit | std::ifstream::badbit );
  try {

    fichier << "Speed calibration : " << speed.x << " " << speed.y << " " << speed.z << std::endl;
    fichier << "Acceleration calibration : " << accel.x << " " << accel.y << " " << accel.z << std::endl;
    fichier << "Yaw calibration : " << 0 << std::endl;

  } catch(std::istream::failure e) {
    ROS_ERROR("Cannot save calibration configuration to %s because %s", absFile.c_str(), e.what());
    return false;
  }

  return true;
}
bool CommandeDrone::loadCalibration(const std::string& file)
{
  Coords speed, accel;
  float yaw;

  std::string absFile = file;
  resolveHome(absFile);

  std::ifstream fichier(absFile);
  if (!fichier)
  {
    ROS_WARN("Could not open file %s", absFile.c_str());
    return false;
  }

  fichier.exceptions( std::ifstream::failbit | std::ifstream::badbit );

  try {
    fichier.ignore(256, ':');

    fichier >> speed.x;
    fichier >> speed.y;
    fichier >> speed.z;

    fichier.ignore(256, ':');

    fichier >> accel.x;
    fichier >> accel.y;
    fichier >> accel.z;

    fichier.ignore(256, ':');

    fichier >> yaw;
  } catch (std::ifstream::failure e) {
    ROS_ERROR("Cannot load calibration configuration from %s because %s", absFile.c_str(), e.what());
    return false;
  }

  speedCalib = speed;
  accelCalib = accel;
  ROS_INFO("Loaded calibration file : speed (%.2f, %.2f, %.2f), acceleration (%.2f, %.2f, %.2f) and yaw (%.2f)", speed.x, speed.y, speed.z, accel.x, accel.y, accel.z, yaw);
  return true;
}

bool CommandeDrone::stabilize(bool bypassFlyingVerif)
{
	ROS_INFO("Stabilizating");
  if (! getState().started) {
    ROS_WARN("Communication not started, call init(). Cannot stabilize");
    return false;
  }
  else if (!posCbCalls) {
    ROS_WARN("No position information gathered, cannot stabilize.");
    return false;
  }
  else if (getState().landed) {
    if (!bypassFlyingVerif){
      ROS_WARN("Not flying, cannot stabilize");
      return false;
    }
    else
      ROS_INFO("Not flying, but by-passing fly check");
  }

  auto compenser = [](float speed){
    return - speed;
  };

  float tolerance = 0.005; //unit / s
  Coords speed = (Coords) {tolerance * 2, 0, 0}; //Juste pour entrer dans la boucle

  int lastUpdate = posCbCalls;
  Coords lastPosition = getPosition();
  ros::Time lastTime = ros::Time::now(), startTime = ros::Time::now();

  bool movementAboveTolerance = true;

  //Pour avoir des données qui sont significatives, on lilmite la frequence d'actualisation a 20 Hz
  //Une technique plus pousse serait de faire une moyenne des donnees obtenues precedemment
  ros::Rate maximumPollRate(20);

  //On continue si :
  while (ok()                                               //Si la connection est valide et que l'utilisateur n'a pas interrompu le programme
      && ((
        movementAboveTolerance                             //+ si on est toujours pas stable
        && ros::Time::now() - lastTime < ros::Duration(1)   //+ si on a toujours des nouvelles frâiches de ROS
        && ros::Time::now() - startTime < ros::Duration(1)  //+ si ça fait pas trop longtemps qu'on essaye
      ) || ros::Time::now() - startTime < ros::Duration(.5)   //ou moins de 1 secondes (minimum 1 sec calibration)
      ))
  {
    maximumPollRate.sleep();

    //On a pas de nouvelle info de position
    if (lastUpdate == posCbCalls)
      continue;

    movementAboveTolerance = false;

    Coords position = getPosition();
    ros::Time time = ros::Time::now();

    speed.x = (position.x - lastPosition.x) / (time - lastTime).toSec();
    speed.y = (position.y - lastPosition.y) / (time - lastTime).toSec();
    speed.z = (position.z - lastPosition.z) / (time - lastTime).toSec();

    logDebug("Computed speed " + std::to_string(speed.x) + " " + std::to_string(speed.y) + " " + std::to_string(speed.z) + " (unit/s)");

    accelerationLock.lock();
    Coords newAccel = accelSetPoint;
    accelerationLock.unlock();

    if (std::abs(speed.x) > tolerance)
    {
      movementAboveTolerance = true;
      newAccel.x += compenser(speed.x);
    }
    if (std::abs(speed.y) > tolerance)
    {
      movementAboveTolerance = true;
      newAccel.y += compenser(speed.y);
    }
    if (std::abs(speed.z) > tolerance)
    {
      movementAboveTolerance = true;
      newAccel.z += compenser(speed.z);
    }

    setAcceleration(newAccel);

    lastUpdate = posCbCalls;
    lastPosition = position;
    lastTime = time;
  }

  if (!movementAboveTolerance) //calibration reussie
  {
    ROS_INFO("Stabilized !");
    accelerationLock.lock();
    accelCalib = accelSetPoint;
    accelerationLock.unlock();
  }
  else {
    ROS_ERROR("Aborted stabilization due to error or not usual long duration");
    logDebug("ok flag = " + std::to_string(ros::ok()) + ", lastTime = " + std::to_string((ros::Time::now() - lastTime).toSec()) + " duration = " + std::to_string((ros::Time::now() - startTime).toSec()));
    return false;
  }


  return true;
}

bool CommandeDrone::takeoff(int height, bool usePos)
{
  if (! getState().started) {
    ROS_WARN("Communication not started, call init(). Cannot takeoff");
    return false;
  }
  else if (!posCbCalls) {
    ROS_WARN("No position calibration, call doPositionCalibration() before takeoff.");
    return false;
  }
  else if (!getState().landed) {
    ROS_WARN("Not in landed state, cannot takeoff");
    return false;
  }

  float acceleration = 0.5;
  float tolerance = 0.1;
  float lastHeight = getPosition().y;
  int lastUpdate = posCbCalls;
  bool reachedTarget = false;

  ros::Time start = ros::Time::now();

  if (usePos)
  {
    setFlying();
    //On devrit n'envoyer qu'un setPoint en hauteur (IGNORE_POS_X|IGNORE_POS_Y)
    Coords pos = getPosition();
    pos.z = height;
    setPosition(pos);
  }

  while (ok() && !reachedTarget && ros::Time::now() - start < ros::Duration(4))
  {
    while (posCbCalls == lastUpdate) {
      ros::Duration(.05).sleep();
    }

    if (std::abs(height - getPosition().y) <= tolerance)
      reachedTarget = true;
    else if (!usePos && getPosition().y - lastHeight <= 0) // On en entrain de descendre ou de ne pas progresser
    {
      accelerationLock.lock();
      accelSetPoint.z += 0.02;
      accelerationLock.unlock();
    }

    lastHeight = getPosition().y;
    lastUpdate = posCbCalls;
  }

  stateLock.lock();
  currentState.landed = false;
  stateLock.unlock();

  return usePos || stabilize();
}

void CommandeDrone::setFlying()
{
  stateLock.lock();
  currentState.landed = false;
  stateLock.unlock();
}

bool CommandeDrone::land(int height, bool usePos)
{
  if (! getState().started) {
    ROS_WARN("Communication not started, call init(). Cannot land");
    return false;
  }
  else if (getState().landed) {
    ROS_WARN("Land commande in landed state (will execute land command anyway)");
  }

  float tolerance = 0.1;
  ros::Time start = ros::Time::now();
  ros::Duration maxTime = ros::Duration(8);

  if (usePos)
  {
    Coords pos = getPosition();
    pos.z = height;
    setPosition(pos);
  }

  if (!usePos && !stabilize(true))
  {
    ROS_ERROR("Cannot stabilize ! Shutting down motors");
    if (ok()) setAcceleration((Coords){0, 0, 0});
    return false;
  }

  float acceleration = accelCalib.z - 0.1;

  int noEvolutionCounter = 0;
  float lastHeight = getPosition().y, lastNoEvHeight = getPosition().y;
  float coeffAcceleration = 0.1;
  int lastUpdate = posCbCalls;
  bool reachedLand = false;


  //TODO: add adaptative acceleration
  while (ok() && !reachedLand && noEvolutionCounter < 30 && ros::Time::now() - start < maxTime)
  {
    while (posCbCalls == lastUpdate) {
      ros::Duration(.05).sleep();
    }

    if (std::abs(height - getPosition().y) <= tolerance)
      reachedLand = true;
    else if (std::abs(lastNoEvHeight - getPosition().y) <= 0.01) //On stagne encore par rapport à la position intiale
      noEvolutionCounter++;
    else if (std::abs(lastHeight - getPosition().y) <= 0.01) //On stagne
    {
      noEvolutionCounter = 0;
      lastNoEvHeight = getPosition().y;
    }
    else if (!usePos && getPosition().y - lastHeight > 0) // On en entrain de monter !
    {
      accelerationLock.lock();
      accelSetPoint.z -= 0.05;
      accelerationLock.unlock();
    }

    lastHeight = getPosition().y;
    lastUpdate = posCbCalls;
  }

  if (!usePos) setAcceleration((Coords){0, 0, 0});

  stateLock.lock();
  currentState.landed = true;
  stateLock.unlock();

  return true;
}


bool CommandeDrone::setMode(const std::string& mode)
{
  if (getState().mode == mode)
    return true;

  SetMode setModeRequest;
  setModeRequest.request.base_mode = 0;
  setModeRequest.request.custom_mode = mode;
  setModeSrv.call(setModeRequest);

  if (setModeRequest.response.mode_sent)
    logDebug("Sent mode " + mode);
  else
    ROS_WARN("Failed to send mode %s", mode.c_str());

  return setModeRequest.response.mode_sent;
}


bool CommandeDrone::setFCUParam(const std::string& param, int value)
{
  return setFCUParam(param, value, 0);
}

bool CommandeDrone::setFCUParam(const std::string& param, double value)
{
  return setFCUParam(param, 0, value);
}

bool CommandeDrone::setFCUParam(const std::string& param, int iValue, double dValue)
{
  ParamSet data;
  data.request.value.integer = iValue;
  data.request.value.real = dValue;
  data.request.param_id = param;

  setParamSrv.call(data);

  return data.response.success ;
}

double CommandeDrone::getFCUParam(const std::string& param)
{
  ParamGet data;
  data.request.param_id = param;

  getParamSrv.call(data);

  int i = data.response.value.integer;
  double d = data.response.value.real;

  //Si integer = 0 alors value = real sinon value = integer
  //Si erreur => -1
  return data.response.success ? (i ? i : d) : -1;
}

bool CommandeDrone::applyParametersFile(const std::string& filename)
{
  std::string absFile = filename;
  resolveHome(absFile);
  std::ifstream fichier(absFile);
  if (!fichier)
  {
    ROS_ERROR_STREAM("Cannot open file " << absFile);
    return false;
  }

  fichier.exceptions( std::ifstream::badbit );

  std::regex  commentRegex("\\s*#.*"),
              keyValueRegex("\\s*param set\\s+([a-zA-Z0-9_\\-]*)\\s+([+\\-]?[0-9.]+)\\s*");
  int nb = 0, total = 0;
  try {
    std::string line;
    while (getline(fichier, line)){

      //Ignore empty or comment lines
      if (!line.size() || std::regex_match(line, commentRegex)) continue;

      std::smatch results;
      if (!std::regex_match(line, results, keyValueRegex))
        ROS_WARN_STREAM("Invalid configuration line in file " << absFile << " : " << line);
      else if (results.size() - 1 != 2) // 2 correspondances attendues (la correspondance globale est aussi retournee)
        ROS_WARN_STREAM("Invalid syntax in file " << absFile << " : " << line);
      else {
        std::string key = results[1], value = results[2];
        bool isDouble = value.find('.') != std::string::npos;
        double dValue = std::atof(value.c_str());

        bool ok;
        if (getFCUParam(key) == dValue)
          ok = true;
        else if (!isDouble) //Number is an int
            ok = setFCUParam(key, (int) dValue);
        else
            ok = setFCUParam(key, dValue);

        if (!ok)
          ROS_ERROR_STREAM("Cannot set value " << dValue << " for parameter " << key);
        else
          nb++;

        total++;
      }
    }
  } catch(std::istream::failure e) {
    ROS_ERROR_STREAM("Cannot apply parameters from file " << absFile << " because " << e.what());
    return false;
  }

  ROS_INFO_STREAM("Applied " << nb << " parameters of " << total << " from file " << absFile);
  return true;
}

bool CommandeDrone::tryArm(bool arm)
{
  if (getState().armed == arm)
    return arm;

  CommandBool armingRequest;
  armingRequest.request.value = arm;

  armSrv.call(armingRequest);

  if (armingRequest.response.success)
    logDebug(std::string("Successfully ") + (armingRequest.request.value ? "armed" : "disarmed"));
  else
    ROS_WARN("Failed to %s", armingRequest.request.value ? "arm" : "disarm");

  return armingRequest.request.value ^ (!armingRequest.response.success);
}

bool CommandeDrone::arm(bool arm)
{
  int tries = 0, maxTries = 30;

  while(tries++ < maxTries && arm != tryArm(arm))
  {
    ros::Duration(0.2f).sleep();
  }
  if (tries >= maxTries) ROS_WARN("Tried %d times to arm, cannot arm ! ", maxTries);
  return tries < maxTries;
}

bool CommandeDrone::goToPosition(const Coords& position, const ros::Duration& timeout, float delta)
{
    //On check si le drone est en vol, sinon on refuse de bouger
    //Ce check est fait dans setPosition, mais ne pas le faire ici obligerait à attendre pour rien jusqu'au timeout
    if (getState().landed) {
        ROS_ERROR("Drone in landed state, rejected position call");
        return false;
    }

    //On defini la position voulue
    setPosition(position);

    //On attend qu'on y soit arrive, pendatn max timeout secondes
    ros::Time debut = ros::Time::now();
    double distanceSq, deltaSq = delta * delta;
    bool timedOut;

    do
    {
        timedOut = ros::Time::now() - debut >= timeout;

        //On calcule la distance
        Coords currentPosition = getPosition();
        double  dx = position.x - currentPosition.x,
                dy = position.y - currentPosition.y,
                dz = position.z - currentPosition.z;
        distanceSq = dx * dx + dy * dy + dz * dz;

    } while (distanceSq > deltaSq && !timedOut);

    return !timedOut;
}

//Defini la position/vitesse/acceleration voulue avec un vecteur en 3D
void CommandeDrone::setPosition(const Coords& position)
{
  if (getState().landed) {
    ROS_ERROR("Drone in landed state, rejected position call");
    return;
  }

  positionLock.lock();

  positionSetPoint = position;

  positionLock.unlock();

  setInfoSent(true, false, false, sendYaw);
  //ROS_INFO("Set position value : (%f;%f;%f)", position.x, position.y, position.z);
}

void CommandeDrone::setSpeed(const Coords& linear)
{
  if (getState().landed) {
    ROS_ERROR("Drone in landed state, rejected speed call");
    return;
  }
  speedLock.lock();

  speedSetPoint = linear;

  speedLock.unlock();

  setInfoSent(false, true, false, sendYaw);
  //ROS_INFO("Set speed value : (%f;%f;%f)", linear.x, linear.y, linear.z);
}

void CommandeDrone::setYaw(float yaw, float angularVelocity)
{
  if (getState().landed) {
    ROS_ERROR("Drone in landed state, rejected yaw call");
    return;
  }
  yawLock.lock();

  yawSetPoint = yaw;
  yawRate = angularVelocity;

  yawLock.unlock();

  setInfoSent(sendPos, sendSpeed, sendAcceleration, angularVelocity != 0);
//  ROS_INFO("Set angular speed value : (%f;%f;%f)", angular.x, angular.y, angular.z);
}

void CommandeDrone::setAcceleration(const Coords& acceleration)
{

  if (getState().landed) {
    ROS_ERROR("Drone in landed state, rejected acceleration call");
    return;
  }
  accelerationLock.lock();

  accelSetPoint = acceleration;

  accelerationLock.unlock();

  setInfoSent(false, false, true, sendYaw);

//  ROS_INFO("Set acceleration value : (%f;%f;%f)", acceleration.x, acceleration.y, acceleration.z);
}

void CommandeDrone::setInfoSent(bool position, bool speed, bool accel, bool yaw)
{
  sendPos = position;
  sendSpeed = speed;
  sendAcceleration = accel;
  sendYaw = yaw;
}


void CommandeDrone::enableDegugLog(bool log)
{
  debugLog = log;
}

void CommandeDrone::logDebug(const std::string& str)
{
  if (debugLog)
    ROS_INFO("%s", str.c_str());
}

void CommandeDrone::setEstimationPreferences(bool estimateAsPosProvider, bool useMOCAP)
{
  if (estimateAsPosProvider) {
    visionIsPositionProvider = true;
    posSub.shutdown();
  } else {
    visionIsPositionProvider = false;
    posSub = nodeHandle.subscribe<geometry_msgs::PoseStamped>("mavros/local_position/pose", 1, &CommandeDrone::posCb, this);
  }
  estimationUseMOCAP = useMOCAP;
}

void CommandeDrone::registerEstimatorTopic(const std::string& topic, bool isStamped)
{
  if (isStamped)
    estimatedPosSub = nodeHandle.subscribe<geometry_msgs::PoseStamped>(topic, 1, &CommandeDrone::setEstimatedPositionStamped, this);
  else
    estimatedPosSub = nodeHandle.subscribe<geometry_msgs::Pose>(topic, 1, &CommandeDrone::setEstimatedPosition, this);
}

void CommandeDrone::setEstimatedPositionStamped(const geometry_msgs::PoseStampedConstPtr& posePtr)
{
  setEstimatedPosition(*posePtr);
}

void CommandeDrone::setEstimatedPosition(const geometry_msgs::PoseConstPtr& posePtr)
{
  geometry_msgs::PoseStamped poseStamped;
  poseStamped.pose = *posePtr;
  poseStamped.header.stamp = ros::Time::now();
  setEstimatedPosition(poseStamped);
}

void CommandeDrone::setEstimatedPosition(const geometry_msgs::PoseStamped& pose)
{
  geometry_msgs::PoseStamped poseStamped = pose;
  poseStamped.header.frame_id = "map";

  if (!estimationUseMOCAP)
    visionPosePub.publish(poseStamped);
  else
    mocapPosePub.publish(poseStamped);


  if (visionIsPositionProvider)
    posCb(boost::make_shared<geometry_msgs::PoseStamped>(poseStamped));
}


void CommandeDrone::batteryCb(const sensor_msgs::BatteryStateConstPtr& batteryState)
{
  stateLock.lock();
  currentState.connected = true;
  currentState.battery = batteryState->percentage;
  stateLock.unlock();
}
void CommandeDrone::stateCb(const StateConstPtr& state)
{
  stateLock.lock();
  currentState.connected = true;
  currentState.mode = state->mode;
  currentState.armed = state->armed;
  stateLock.unlock();
}


void CommandeDrone::posCb(const geometry_msgs::PoseStampedConstPtr& pos)
{
  stateLock.lock();
  currentState.connected = true;
  currentState.position.x = pos->pose.position.x;
  currentState.position.y = pos->pose.position.y;
  currentState.position.z = pos->pose.position.z;

  auto orientation = Eigen::Quaternionf(pos->pose.orientation.w, pos->pose.orientation.x, pos->pose.orientation.y, pos->pose.orientation.z)
                      .toRotationMatrix().eulerAngles(0, 1, 2); //Angle order

  currentState.orientation.x = orientation[0];
  currentState.orientation.y = orientation[1];
  currentState.orientation.z = orientation[2];
  stateLock.unlock();
  posCbCalls++;
}

void CommandeDrone::speedCb(const geometry_msgs::TwistStampedConstPtr& speed)
{
  stateLock.lock();
  currentState.connected = true;
  currentState.speed.x = speed->twist.linear.x;
  currentState.speed.y = speed->twist.linear.y;
  currentState.speed.z = speed->twist.linear.z;
  stateLock.unlock();
}

void CommandeDrone::imuCb(const sensor_msgs::ImuConstPtr& imuData)
{
  stateLock.lock();
  currentState.connected = true;
  currentState.acceleration.x = imuData->linear_acceleration.x;
  currentState.acceleration.y = imuData->linear_acceleration.y;
  currentState.acceleration.z = imuData->linear_acceleration.z;
  stateLock.unlock();
}

bool CommandeDrone::checkMAVROSConnection()
{
  std::vector<std::string> nodes;
  ros::master::getNodes(nodes);
  //S'il existe un noeud mavros, c'est bon
  if (std::find(nodes.begin(), nodes.end(), "/mavros") != nodes.end())
    return true;

  ROS_WARN("Could not find /mavros node, checking for connection...");

  //Si jamais le noeud a été renommé ou est dans un autre namespace, on check si on a déjà reçu des infos
  ros::Duration timeout(5);
  ros::Time startTime = ros::Time::now();
  while(ros::Time::now() - startTime <= timeout && !getState().connected)
  {
    ros::Duration(.1).sleep();
  }

  return getState().connected;
}

bool CommandeDrone::ok()
{
  return getState().started && ros::ok() && !shouldExit && !interrupt_flag;
}

void CommandeDrone::runCommunicationThread()
{
  const unsigned RATE = 20;
  ros::Rate rateLimiter(RATE);
  ros::Time lastOutput, lastInactiveWarning;

  ROS_INFO("Started communication loop with rate = %d Hz", RATE);
  int loopNum = 0;
  while(ok())
  {
    if (interrupt_flag)
    {
      ROS_INFO("User interrupt reached communication thread");
      interrupt_flag = false;
      shouldExit = true;
      break;
    }

    if (sendPos || sendSpeed || sendAcceleration || sendYaw)
    {
      if (loopNum < 60)
        setMode(PX4_MODE_OFFBOARD);

      PositionTarget setpoint;

      if (sendPos){
        positionLock.lock();
        setpoint.position.x = positionSetPoint.x;// + posCalib.x;
        setpoint.position.y = positionSetPoint.y;// + posCalib.y;
        setpoint.position.z = positionSetPoint.z;// + posCalib.z;
        positionLock.unlock();
      }
      else if (sendSpeed){
        speedLock.lock();
        setpoint.velocity.x = speedSetPoint.x + speedCalib.x;
        setpoint.velocity.y = speedSetPoint.y + speedCalib.y;
        setpoint.velocity.z = speedSetPoint.z + speedCalib.z;
        speedLock.unlock();
      }
      else if (sendAcceleration){
        accelerationLock.lock();
        setpoint.acceleration_or_force.x = accelSetPoint.x + accelCalib.x;
        setpoint.acceleration_or_force.y = accelSetPoint.y + accelCalib.y;
        setpoint.acceleration_or_force.z = accelSetPoint.z + accelCalib.z;
        accelerationLock.unlock();
      }

      if (sendYaw)
      {
        yawLock.lock();
        setpoint.yaw = yawSetPoint;
        setpoint.yaw_rate = yawRate;
        yawLock.unlock();
      }

      unsigned short mask = (sendPos ? 0 : PositionTarget::IGNORE_PX | PositionTarget::IGNORE_PY | PositionTarget::IGNORE_PZ)   |
                            (sendSpeed ? 0 : PositionTarget::IGNORE_VX | PositionTarget::IGNORE_VY | PositionTarget::IGNORE_VZ) |
                            (sendAcceleration ? 0 : PositionTarget::IGNORE_AFX | PositionTarget::IGNORE_AFY | PositionTarget::IGNORE_AFZ) |
                            (sendYaw ? 0 : /*PositionTarget::IGNORE_YAW |*/ PositionTarget::IGNORE_YAW_RATE);

	    mask |= PositionTarget::IGNORE_YAW;
      //  mask |= PositionTarget::FORCE; // Force in acceleration_or_force vector

      setpoint.coordinate_frame = 0; //PositionTarget::FRAME_BODY_OFFSET_NED; //or FRAME_LOCAL_NED, FRAME_LOCAL_OFFSET_NED, FRAME_BODY_NED
      setpoint.type_mask = mask;

      setPointPub.publish(setpoint);


      //logDebug("SENDED SETPOINT :");
      //logDebug(describeSetPoint(setpoint));

      loopNum++;
    }
    else {
      if (ros::Time::now() - lastInactiveWarning > ros::Duration(1))
      {
        ROS_WARN("No command defined, FCU will automatically quit offboard mode");
        lastInactiveWarning = ros::Time::now();
      }

      loopNum = 0;
    }

    rateLimiter.sleep();

    if (ros::Time::now() - lastOutput > ros::Duration(5))
    {
      lastOutput = ros::Time::now();
      ROS_INFO("-------------- STATE --------------");
      ROS_INFO("Conected with MAVROS : %s", currentState.connected ? "yes" : "no");
      if (currentState.connected) {
        ROS_INFO("Armed %s", currentState.armed ? "true" : "false");
        ROS_INFO("Mode %s", currentState.mode.c_str());
        ROS_INFO("Battery left %d%%", (int)(currentState.battery * 100));
        ROS_INFO("Estimated position (%.3f ; %.3f ; %.3f )", currentState.position.x, currentState.position.y, currentState.position.z);
        ROS_INFO("Estimated orientation (%.3f ; %.3f ; %.3f )", currentState.orientation.x, currentState.orientation.y, currentState.orientation.z);
        ROS_INFO("Estimated speed (%.3f ; %.3f ; %.3f )", currentState.speed.x, currentState.speed.y, currentState.speed.z);
        ROS_INFO("Estimated acceleration (%.3f ; %.3f ; %.3f )", currentState.acceleration.x, currentState.acceleration.y, currentState.acceleration.z);
      }
      ROS_INFO("------------------------------------");
    }
  }
  logDebug("Ended communication loop. Ros status : " + std::to_string(ros::ok()) + ", thread exit flag : " + std::to_string(shouldExit));
}

//Retourne le position et la vitesse actuelle du drone
Coords CommandeDrone::getPosition()
{
  return getState().position;
}

Coords CommandeDrone::getSpeed()
{
  return getState().speed;
}

Coords CommandeDrone::getAcceleration()
{
  return getState().acceleration;
}

Coords CommandeDrone::getOrientation()
{
  return getState().orientation;
}

//Retourne l'etat du drone
DroneState CommandeDrone::getState()
{
  stateLock.lock();
  DroneState state = currentState;
  stateLock.unlock();
  return state;
}
