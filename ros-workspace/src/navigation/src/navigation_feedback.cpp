#include <ros/ros.h>
#include <geometry_msgs/Vector3.h>
#include <navigation/commande_drone.hpp>

bool areEquals(double a, double b)
{
  return a == b; //On peut aussi ajouter de la tolérance ?
}

bool areEquals(const Coords& a, const Coords& b)
{
  return areEquals(a.x, b.x) && areEquals(a.y, b.y) && areEquals(a.z, b.z);
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "navigation_feedback");//, ros::init_options::NoSigintHandler);
  ros::NodeHandle nh;

  ros::Publisher speedPub = nh.advertise<geometry_msgs::Vector3>("/test_estimated_vel", 5);
  ros::Publisher accelPub = nh.advertise<geometry_msgs::Vector3>("/test_estimated_accel", 5);

  CommandeDrone commande(nh, false);

  float coeff = 1;
  nh.getParam("coeff", coeff);

/*  if (!commande.init())
  {
    ROS_ERROR("Impossible d'initialiser le guidage du drone !");
    return 1;
  }
  ros::Duration(2).sleep();
  commande.setFlying();
  commande.setPosition((Coords){0, 0, .2});
  ROS_INFO("Going to %.2f", .2 + commande.getPosition().z);
*/

  std::string estimatorTopic = nh.resolveName("estimated_pose");

  commande.setEstimationPreferences(true, false);
  commande.registerEstimatorTopic(estimatorTopic, true);

  /* REPERE de DSO :
    Quaternion.x = pitch
    Quaternion.y = yaw
    Quaternion.z = roll
    Quaternion.w = real coefficient

    pose.x = Gauche -> Droite
    pose.y = Derrière -> Devant
    pose.z = Bas -> Haut

    REPERE de CommandeDrone :
    Rotation axe x = pitch
    Rotation axe y = roll
    Rotation axe z = yaw

    pose.x = Gauche -> Droite
    pose.y = Derrière -> Devant
    pose.z = Bas -> Haut

    REPERE du topic vision_pose : ????

    REPERE du topic local_position: ????

    Différentes frames: ????

    Conversion Quaternion -> Euler de Eigen: ?


    REPERE DSO (pos, quaternion)
    -> REPERE vision_pose(pos, quaternion) [TOUT OK]
    -> REPERE local_position(pos, quaternion) [OK Z, FAIL X, Y, ROTATION]
    -> REPERE CommandeDrone(pos, quaternion) [OK Z]
    -> REPERE CommandeDrone(pos, euler) [OK Z]

    => x,y : FAIL, z : OK, rotation : FAIL
    */

  Coords lastPosition = commande.getPosition(), lastSpeed = {0, 0, 0};
  ros::Rate limiter = ros::Rate(30);
  ros::Time last = ros::Time::now();
  bool hasSpeed = false;

  while(ros::ok()) {
  /*  ros::Duration(3).sleep();
    , orientation = commande.getOrientation();
    ROS_INFO("Position %.3f %.3f %.3f", position.x, position.y, position.z);
    ROS_INFO("Yaw : %.3f, Pitch : %.3f, Roll : %.3f", orientation.z, orientation.x, orientation.y);*/
    Coords position = commande.getPosition();
    if (!areEquals(position, lastPosition))
    {
      ros::Time now = ros::Time::now();
      float duration = (now - last).toSec();

      Coords speed = {
        coeff * (position.x - lastPosition.x) / duration,
        coeff * (position.y - lastPosition.y) / duration,
        coeff * (position.z - lastPosition.z) / duration
      };

      geometry_msgs::Vector3 vitesse;
      vitesse.x = speed.x;
      vitesse.y = speed.y;
      vitesse.z = speed.z;
      speedPub.publish(vitesse);

      if (hasSpeed)
      {
        geometry_msgs::Vector3 accel;
        accel.x = (speed.x - lastSpeed.x) / duration;
        accel.y = (speed.y - lastSpeed.y) / duration;
        accel.z = (speed.z - lastSpeed.z) / duration;
        accelPub.publish(accel);
      }

      last = now;
      lastSpeed = speed;
      hasSpeed = true;
    }
    lastPosition = position;
    limiter.sleep();
  }

  ROS_INFO("Exiting...");
  return 0;
}
