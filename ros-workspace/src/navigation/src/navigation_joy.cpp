#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <navigation/commande_drone.hpp>

#include <algorithm>
#include <cmath>
#include <signal.h>

#define MATH_PI 3.14159265358979323846

#define MAX_VALUE 0.3

#define MIN_LINEAR_INPUT -1
#define MAX_LINEAR_INPUT 1

#define MIN_ANGULAR_INPUT -1
#define MAX_ANGULAR_INPUT 1
#define MAX_ANGULAR_SPEED 1


/**
* Use joy for joystick control
*/

Coords acceleration = (Coords){0, 0, 0}, rotation = (Coords){0, 0, 0}, trim = (Coords){0, 0, 0};
int axis_x = 1, axis_y = 0, axis_z = 3, axis_rotation_z = 2, button_land = 1, button_go = 0, button_takeoff = 2, button_stabilize = 4;
int axis_x_calib = 4, axis_y_calib = 5, button_z_calib_moins = 10, button_z_calib_plus = 11;

bool land, started;

bool doStabilize = false;

std::mutex values;

int sign(float x)
{
  return (x > 0) - (x < 0);
}

float map(float x, float minA, float maxA, float minB, float maxB)
{
  return minB + (maxB - minB) * ((x - minA) / (maxA - minA));
}

float mapAxis(const sensor_msgs::Joy& joystick, unsigned axisIndex, float minInput, float maxInput, float minOutput, float maxOutput, bool inv = false)
{
  if (axisIndex == -1) return 0;
  float value = map(joystick.axes[axisIndex], minInput, maxInput, minOutput, maxOutput);
  return inv ? - value : value;
}

//TODO : use CommandeDrone getSate().landed
//TODO: use CommandeDrone takeoff and land

void processCommand(const sensor_msgs::JoyConstPtr& joystick)
{
  float xAccelMapped = mapAxis(*joystick, axis_x, MIN_LINEAR_INPUT, MAX_LINEAR_INPUT, -MAX_VALUE / 2, MAX_VALUE / 2, true),
        yAccelMapped = mapAxis(*joystick, axis_y, MIN_LINEAR_INPUT, MAX_LINEAR_INPUT, -MAX_VALUE / 2, MAX_VALUE / 2, true),
        zAccelMapped = mapAxis(*joystick, axis_z, MIN_LINEAR_INPUT, MAX_LINEAR_INPUT, 0, MAX_VALUE * 2),
        zRotationMapped = mapAxis(*joystick, axis_rotation_z, MIN_ANGULAR_INPUT, MAX_ANGULAR_INPUT, - MAX_ANGULAR_SPEED, MAX_ANGULAR_SPEED);

  float xTrim = mapAxis(*joystick, axis_x_calib, MIN_LINEAR_INPUT, MAX_LINEAR_INPUT, - 0.004, 0.004, true),
        yTrim = mapAxis(*joystick, axis_y_calib, MIN_LINEAR_INPUT, MAX_LINEAR_INPUT, - 0.004, 0.004),
        zTrim = button_z_calib_moins == -1 || button_z_calib_plus == -1 ? 0 :
                    joystick->buttons[button_z_calib_moins] ? -0.002 :
                    joystick->buttons[button_z_calib_plus] ? 0.002 : 0;

  //Si on est en phase d'atterissage ou qu'on a pas decolle
  if (!started || land)
  {
    //Si on veut (re)prendre le controle
    if (button_go == -1 || joystick->buttons[button_go])
    {
      if (!started && zAccelMapped != 0)
      {
        ROS_WARN("Throttle value is non-zero, refusing to start");
      }
      else if (land && std::abs(zAccelMapped - acceleration.z) > 0.3)
      {
        ROS_WARN("Throttle value to far from current (current : %.2f, requested : %.2f), refusing to start", acceleration.z, zAccelMapped);
      }
      else {
        started = true;
        land = false;
      }
    }

    //Si on est en phase d'atterissage on ne veut que decélerer verticalement
    values.lock();
    rotation.z = 0;
    acceleration.x = acceleration.y = 0;
    values.unlock();
  }
  else {

    values.lock();
    //Si on veut atterir
    if (button_land != -1 && joystick->buttons[button_land])
    {
      land = true;
      //On limite direct l'acceleration verticale
      if (acceleration.z > 0.5)
        acceleration.z = 0.5;
    }
    else if (button_stabilize != -1 && joystick->buttons[button_stabilize])
    {
      doStabilize = true;
    }

    acceleration.x = xAccelMapped;
    acceleration.y = yAccelMapped;
    acceleration.z = zAccelMapped;

    rotation.z = zRotationMapped;

    if (acceleration.x > MAX_VALUE) acceleration.x = MAX_VALUE;
    else if (acceleration.x < - MAX_VALUE) acceleration.x = - MAX_VALUE;

    if (acceleration.y > MAX_VALUE) acceleration.y = MAX_VALUE;
    else if (acceleration.y < - MAX_VALUE) acceleration.y = - MAX_VALUE;

    if (acceleration.z > MAX_VALUE * 2) acceleration.z = MAX_VALUE * 2;
    else if (acceleration.z < 0) acceleration.z = 0;

    if (rotation.z > MAX_VALUE) rotation.z = MAX_VALUE;
    else if (rotation.z < - MAX_VALUE) rotation.z = - MAX_VALUE;

    trim.x = xTrim;
    trim.y = yTrim;
    trim.z = zTrim;

    values.unlock();


    char buffer[21];


    char axisName[4][3] = {"X", "Y", "RZ", "Z"};
    for (int i = 0; i < 4; i++)
    {
      int percentage = 100 * (joystick->axes[i] + 1) / 2;
      int nb = percentage / 5;

      int n;
      for (n = 0; n < nb; n++) buffer[n] = '#';
      buffer[n] = 0;


      ROS_INFO("Axis %s : %-20s %d %%", axisName[i], buffer, percentage);
    }
  }

}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "navigation_joy", ros::init_options::NoSigintHandler);
  ros::NodeHandle nh;
  bool useVisionAsReference = true;

  started = land = false;

  std::string visionPoseTopic = "/dso/pose";
  std::string joyTopic = nh.resolveName("/joy");

  ROS_INFO("Utilisation du topic %s", joyTopic.c_str());


  nh.getParam("x_axis", axis_x);
  nh.getParam("y_axis", axis_y);
  nh.getParam("z_axis", axis_z);
  nh.getParam("yaw_axis_index", axis_rotation_z);
  nh.getParam("land_button", button_land);
  nh.getParam("start_button", button_go);
  nh.getParam("takeoff_button", button_takeoff);
  nh.getParam("stabilize_button", button_stabilize);
  nh.getParam("use_vision", useVisionAsReference);

  ROS_INFO("Joystick configuration : (-1 means desactivated)");
  ROS_INFO("  * X axis : joystick axis number %d", axis_x);
  ROS_INFO("  * Y axis : joystick axis number %d", axis_y);
  ROS_INFO("  * Z axis : joystick axis number %d", axis_z);
  ROS_INFO("  * Yaw : joystick axis number %d", axis_rotation_z);
  ROS_INFO("  * Land button : joystick button number %d", button_land);
  ROS_INFO("  * Start button : joystick button number %d", button_go);
  ROS_INFO("  * Takeoff button : joystick button number %d", button_takeoff);
  ROS_INFO("  * Use vision position as reference (if present) : %s", useVisionAsReference ? "yes" : "no");

  CommandeDrone commande(nh);

  commande.enableDegugLog();

  std::vector<ros::master::TopicInfo> topics;
  ros::master::getTopics(topics);
  if (std::any_of(topics.cbegin(), topics.cend(), [visionPoseTopic](ros::master::TopicInfo t) {return t.name == visionPoseTopic;})) {
    ROS_INFO("Le topic %s existe, on s'en sert pour l'estimation de position", visionPoseTopic.c_str());
    commande.setEstimationPreferences(useVisionAsReference, false);
    commande.registerEstimatorTopic("/dso/pose", false);
  }

  ROS_INFO("On start le guidage du drone");

  if (!commande.init())
  {
    ROS_ERROR("Impossible d'initialiser le guidage du drone !");
    return 1;
  }

  ros::Subscriber keySubscriber = nh.subscribe<sensor_msgs::Joy>(joyTopic, 1, &processCommand);

  ros::Duration timeElapsed;
  while(commande.ok() && (!land || acceleration.z >= 0)) {
    ros::Time debut = ros::Time::now();

    if (!started) commande.setFlying();
    else {

      values.lock();

      if (land){
        acceleration.z -= .5 * timeElapsed.toSec();
      }
      else if (doStabilize)
      {
        ROS_INFO("Calling stabilization");
        if (!commande.stabilize())
          ROS_WARN("Stabilization failed");

        doStabilize = false;
      }

      commande.setAcceleration(acceleration);
      commande.setYaw(0, rotation.z);

      if (trim.x) commande.setAccelerationTrimX(trim.x);
      if (trim.y) commande.setAccelerationTrimY(trim.y);
      if (trim.z) commande.setAccelerationTrimZ(trim.z);
      trim = (Coords) {0, 0, 0}; //On reset le trim

      values.unlock();

    }

    timeElapsed = ros::Time::now() - debut;
  }

  if (land) ROS_INFO("Landed !");

  ROS_INFO("Exiting...");
  return 0;
}
