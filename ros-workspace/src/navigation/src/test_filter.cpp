#include <ros/ros.h>
#include <sensor_msgs/FluidPressure.h>
#include <std_msgs/Float64.h>
#include <opencv2/opencv.hpp>

#define PRESSURE_0 101325

/**
* Noeud permettant la conversion de la pression du baromètre en altitude
* Applique un flitre puis convertit la valeur en mètres, par rapport au niveau de la mer (pression de référence = 101 325 Pascals)
* Plutôt fiable, mais moins que le filtre de Kalmann étendu utilisé sur le baromètre par PX4 quand le baromètre est source de position verticale
**/

int alpha_int = 500;
double alpha = 0.02; // 0 - 1
double filtered_data_ = 0;

ros::Publisher pub, altitude;

double filter(double raw_input) {
    filtered_data_ = filtered_data_ - (alpha * (filtered_data_ - raw_input));
    return filtered_data_;
}

double pressure2Meters(double pressure)
{
  double temperature = 25;
  return ((pow(PRESSURE_0 / pressure, 1 / 5.257) - 1) * (temperature + 273.15)) / 0.0065;
}

void cb(const sensor_msgs::FluidPressureConstPtr& pressure)
{
    double data = pressure->fluid_pressure;

    if (filtered_data_ < 1)
    {
//	ROS_INFO("INIT filtered_data_ to %.2f", data);
        filtered_data_ = data;
        return;
    }

    alpha = alpha_int / 1000.;
    data = filter(data);

    double meters = pressure2Meters(data);

    std_msgs::Float64 msg;
    msg.data = data;
  ///ROS_INFO("data : %.2f", data);
    pub.publish(msg);
    msg.data = meters;
    altitude.publish(msg);
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "filter_test");
  ros::NodeHandle nh;

  ros::Subscriber sub = nh.subscribe<sensor_msgs::FluidPressure>("/mavros/imu/static_pressure", 1, cb);
  pub = nh.advertise<std_msgs::Float64>("/filtered_data", 1);
  altitude = nh.advertise<std_msgs::Float64>("/altitude", 1);

  cv::namedWindow("param");
  cv::createTrackbar("Alpha", "param", &alpha_int, 1000);


  while (ros::ok()){
    cv::waitKey(30);
    ros::spinOnce();
  }

  return 0;
}
