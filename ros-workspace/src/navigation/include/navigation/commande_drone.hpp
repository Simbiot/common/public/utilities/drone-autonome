#ifndef COMMANDE_DRONE_INCLUDED
#define COMMANDE_DRONE_INCLUDED

#include <thread>
#include <mutex>
#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <mavros_msgs/State.h>
#include <sensor_msgs/BatteryState.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>

//Qualques modes de vol PX4
#define PX4_MODE_OFFBOARD "OFFBOARD"
#define PX4_MODE_MANUAL "MANUAL"
#define PX4_MODE_POSITION "POSITION"

//Quelques parametre et valeurs de paramètres de PX4
#define PX4_PARAM_ID_ESTIMATOR_TYPE "SYS_MC_EST_GROUP"
#define PX4_PARAM_ESTIMATOR_LPE 1
#define PX4_PARAM_ESTIMATOR_EKF2 2

#define PX4_PARAM_ID_EKF2_BITMASK "EKF2_AID_MASK"
#define PX4_PARAM_EKF2_BM_GPS 0x01
#define PX4_PARAM_EKF2_BM_OPTIFLOW 0x02
#define PX4_PARAM_EKF2_BM_INHIBIT_IMU 0x04
#define PX4_PARAM_EKF2_BM_VISION_POS 0x08
#define PX4_PARAM_EKF2_BM_VISION_HEADING 0x10
#define PX4_PARAM_EKF2_BM_MULTIROTOR_DRAG 0x20
#define PX4_PARAM_EKF2_BM_ROTATE_COORDS 0x40
#define PX4_PARAM_EKF2_BM_GPS_HEADING 0x80

#define PX4_PARAM_ID_EKF2_HEIGHT_MODE "EKF2_HGT_MODE"
#define PX4_PARAM_EKF2_HEIGHT_BARO 0
#define PX4_PARAM_EKF2_HEIGHT_GPS 1
#define PX4_PARAM_EKF2_HEIGHT_RANGE 2
#define PX4_PARAM_EKF2_HEIGHT_VISION 3

#define PX4_PARAM_ID_EKF2_DELAY "EKF2_EV_DELAY"
#define PX4_PARAM_ID_EKF2_VISION_GATE "EKF2_EV_GATE"
#define PX4_PARAM_ID_EKF2_VISION_HEADING_NOISE "EKF2_EVA_NOISE"
#define PX4_PARAM_ID_EKF2_VISION_POS_NOISE "EKF2_EVP_NOISE"
#define PX4_PARAM_ID_EFK2_FUSE_BETA "EKF2_FUSE_BETA"

#define PX4_PARAM_ID_EKF2_EV_OFFSET(axis) (std::string("EKF2_EV_POS_") + axis)

//3D point or vector
typedef struct {
  float x; float y; float z;
} Coords;

//Structure for the drone and controller states
typedef struct {
  bool armed; //True if the drone is armed (propellers are turning)
  bool connected; //True if data has been received from MAVROS
  bool started; //True if init() has been called
  bool landed; //True if the drone is landed, false if it is flying
  std::string mode; //Effective fly mode
  Coords position; //Current position reported by the flight controller
  Coords speed; //Current speed reported by the flight controller
  Coords acceleration; //Current acceleration reported by the flight controller
  Coords orientation; //Current attitude (yaw, pitch, roll) reported by the flight controller
  float battery; //Battery percentage (0 to 1)
} DroneState;


//Cette classe permet la communication à un controleur de vol de drone via MAVROS.
//Elle a été conçue pour le contrôleur de vol PX4 avec un drone Intel Aero RTF, mais pourrait marcher avec d'autres drones / controleurs
class CommandeDrone {
public:

  //Construit l'objet de commande du drone. Prend en paramètre le noeud ROS et un booléen indiquant
  //si l'objet doit prendre en main la gestion des interuptions utilisateur (SIGINT), le cas échéant le noeud ROS doit être configurer pour ne pas le gérer (voir http://wiki.ros.org/roscpp/Overview/Initialization%20and%20Shutdown#Initialization_Options)
  CommandeDrone(const ros::NodeHandle& handle, bool sigintHandler = true);
  ~CommandeDrone();

  //Calibre, arme, passe en mode offboard et lance le début du controle du drone
  bool init(const std::string& calibrationFile = DEFAULT_CALIBRATION_FILE, const std::string& parameterFile = DEFAULT_PARAMETER_FILE);
  //Repasse en mode manuel et coupe les moteurs/disarm
  bool exit();

  //Retourne vrai si tout va bien (remplacement de ros::ok() quand l'objet gère l'interruption utilisateur)
  //Tout va bien = init() a été appelé avec succès, l'utilisateur n'a pas envoyé un signal SIGINT (CTRL + C) et aucune erreur fatale dans le noeud ros ni dans l'objet de commande
  bool ok();
  bool checkMAVROSConnection();

  //Defini le mode de vol pour PX4 (https://docs.px4.io/en/flight_modes/), true si succes
  bool setMode(const std::string& mode);

  //Defini un parametre PX4 (https://docs.px4.io/en/advanced_config/parameter_reference.html), le type doit être correct : si le type attendu est un flottant, on doit envoyer un flottant (40.0 au lieu de 40 par exemple)
  bool setFCUParam(const std::string& param, int value);
  bool setFCUParam(const std::string& param, double value);
  //Retourne un parametre de PX4
  double getFCUParam(const std::string& param);

  //Applique un fichier de paramètres au FCU
  //Format attendu : param = valeur (un par ligne, commentaire avec #)
  bool applyParametersFile(const std::string& filename);

  //Arm ou disarm, retourne vrai si arme
  // Essaye 30 fois d'armer avant de renoncer
  bool arm(bool arm = true);
  //Appel non bloquant : si l'arm échoue retourne immédiatemment
  bool tryArm(bool arm);

  //Defini la position/vitesse/attitude/acceleration voulue avec un vecteur en 3D
  //Le controleur de vol doit avoir des informations de position fiables afin d'appliquer ces commandes (DPS, odométrie visuelle, motion capture...)
  //Le drone doit être "en vol" pour utiliser ces méthodes
  void setPosition(const Coords& position);
  void setSpeed(const Coords& linear);
  void setYaw(float yaw, float angularVelocity);
  void setAcceleration(const Coords& acceleration);

  //Cette méthode est équivalente à setPosition, mais attend que le drone ait atteint la position
  //Si le drone n'atteint pas la position avant timeout secondes, la fonction retourne faux
  //Le drone est detecté comme ayant atteint la position voulu s'il en est écarté d'une distance inférieure ou égale à delta mètres
  //Si timeout = 0, alors cette fonction est équivalente à setPosition
  //Si timeout < 0, alors cette fonction attend indéfinimentfloat
  bool goToPosition(const Coords& position, const ros::Duration& timeout = ros::Duration(10), float delta = 0.05);

  //Defini la position estimée, venant d'un algo d'odométrie visuelle ou d'un système de motion capture
  void setEstimatedPosition(const geometry_msgs::PoseStamped& poseStamped);
  //Version utilitaire de la méthode précédente pour être utilisée comme callback pour ROS
  void setEstimatedPosition(const geometry_msgs::PoseConstPtr& posePtr);
  void setEstimatedPositionStamped(const geometry_msgs::PoseStampedConstPtr& posePtr);

  //Si useVisionAsReference est vrai, alors le flux de position de vision/mocap sera considéré comme fournisseur de position
  //Si useMOCAP est vrai alors on envoie le flux sur mocap/pose au lieu de vision_pose/pose
  // Pour PX4 avec l'estimateur EKF2 (par défaut dans les version récentes), ne pas utiliser useMocap = true car ce flux n'est pas traité
  void setEstimationPreferences(bool useVisionAsReference = false, bool useMOCAP = false );

  //Permet de rediriger les informations de position de ce topic vers l'estimateur de position de PX4
  //Si isStamepd = true alors le flux contient des stamps (PoseStamped), sinon il n'en contient pas (Pose)
  void registerEstimatorTopic(const std::string& topic, bool isStamped = true);

  //Attention, ces 3 méthodes ne sont pas testées et ne sont de toute façons pas optimales (ce sont des tests de ce qu'on pouvait arriver à faire)
  //Méthodes permettant de décoller et d'atterir vers la hauteur voulue (normalement pour l'atterissage, hauteur = 0)
  //Ces méthodes permettent de changer l'état du drone (en vol ou à terre)
  //Si usePos est à vrai, les méthodes utiliseront les setpoints du controleur de vol, sinon un algorithme est utilisé avec les informations de controle
  bool takeoff(int height, bool usePose = true);
  bool land(int height = 0, bool usePos = true);

  //Méthodes permettant la stabilisation du drone sur les 3 axes (bypassFlyingVerif permet de stabuiliser même si l'état du drone n'est pas "En vol")
  //met à jour la calibration en acceleration
  bool stabilize(bool bypassFlyingVerif = false);

  //Permet de forcer le mode "en vol"
  void setFlying();

  //Retourne le position et la vitesse actuelle du drone
  Coords getPosition();
  Coords getSpeed();
  Coords getAcceleration();
  Coords getOrientation();

  //Retourne l'etat du drone
  DroneState getState();

  //Enable debug messages
  void enableDegugLog(bool log = true);

  //Calibrate the position according to current position
  //The position set points (setPosition()) are relative to this position
  void doPositionCalibration();

  //Sets the speed and acceleration trims for each axis
  //Change the origin for each axis
  //The speed and acceleration set points (setSpeed() and setAcceleration()) are thus shifted
  void setAccelerationTrimX(float xTrimInc);
  void setAccelerationTrimY(float yTrimInc);
  void setAccelerationTrimZ(float zTrimInc);

  void setSpeedTrimX(float xTrimInc);
  void setSpeedTrimY(float yTrimInc);
  void setSpeedTrimZ(float zTrimInc);

  //Loads and save calibration data (position, speed, acceleration and yaw (not implemented)) from/to a file
  //This is performed automatically at initialization and clean shutdown
  bool saveCalibration(const std::string& file);
  bool loadCalibration(const std::string& file);

private:
  static constexpr const char* DEFAULT_CALIBRATION_FILE = "PKG_HOME/calibration/default.calib";
  static constexpr const char* DEFAULT_PARAMETER_FILE = "PKG_HOME/calibration/default.param";

  ros::NodeHandle nodeHandle;
  ros::AsyncSpinner spinner;

  ros::Publisher setPointPub, visionPosePub, mocapPosePub;
  ros::Subscriber stateSub, batterySub, posSub, speedSub, rawIMUSub, estimatedPosSub;
  ros::ServiceClient armSrv, setModeSrv, getParamSrv, setParamSrv;

  DroneState currentState;

  bool sendPos, sendSpeed, sendAcceleration, sendYaw;
  Coords positionSetPoint, speedSetPoint, accelSetPoint;
  float yawSetPoint, yawRate;

  //Condition de fin de loops multithread : doivent etre volatiles
  volatile int posCbCalls;
  volatile bool shouldExit;

  Coords posCalib, speedCalib, accelCalib;

  std::thread communicationThread;
  std::mutex positionLock, speedLock, yawLock, accelerationLock, stateLock;
  bool debugLog, visionIsPositionProvider, estimationUseMOCAP;

  void runCommunicationThread();
  void batteryCb(const sensor_msgs::BatteryStateConstPtr& batteryState);
  void stateCb(const mavros_msgs::StateConstPtr& state);
  void posCb(const geometry_msgs::PoseStampedConstPtr& pos);
  void speedCb(const geometry_msgs::TwistStampedConstPtr& speed);
  void imuCb(const sensor_msgs::ImuConstPtr& imuData);
  //Quels infos on envoie a mavros
  void setInfoSent(bool position, bool speed, bool accel, bool yaw);

  bool setFCUParam(const std::string& param, int iValue, double dValue);

  void logDebug(const std::string& str);
};


#endif
