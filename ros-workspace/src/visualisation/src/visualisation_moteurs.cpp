
#include <ros/ros.h>
#include <mavros_msgs/RCIn.h>
#include <mavros_msgs/RCOut.h>

void afficherEtat(const mavros_msgs::RCOutConstPtr& motorsData)
{
  const int MIN_CHANNEL = 0, MAX_CHANNEL = 4;
  const int MIN_VALUE = 1190, MAX_VALUE = 2000;

  char buffer[21];

  for (int i = MIN_CHANNEL; i < MAX_CHANNEL; i++)
  {
    int percentage = 100 * (motorsData->channels[i] - MIN_VALUE) / (MAX_VALUE - MIN_VALUE);
    int nb = percentage / 5;

    int n;
    for (n = 0; n < nb; n++) buffer[n] = '#';
    buffer[n] = 0;


    ROS_INFO("Motor %d : %-20s %d %%", i + 1, buffer, percentage);
  }
  ROS_INFO(" ");

}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "visualisation_moteurs");
  ros::NodeHandle nh;

  ros::Subscriber motorsSub = nh.subscribe<mavros_msgs::RCOut>("/mavros/rc/out", 10, afficherEtat);

  ros::spin();

  return 0;
}
