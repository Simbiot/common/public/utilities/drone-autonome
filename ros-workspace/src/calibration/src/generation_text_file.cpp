#include "ros/ros.h"
#include "cv_bridge/cv_bridge.h"
#include <opencv2/opencv.hpp>
#include <sstream>
#include <iostream>
#include <fstream>
using namespace std;

/**
* Ce fichier vise à générer de façon automatique le fichier times.text ainsi qu'un dataset de 1500 images
* Ces derniers sont nécessaires pour générer les autres fichiers de calibrations de notre drone
*/

//Temp d'exposure (en prenant en compte que le rosparam /camera/driver/color_exposure soit = 156 (1000/156))
float exposureTime = 6.41025641;
//Nombre d'images générées
int nbImages = 0;
//Nombre d'images à save
int maxImage = 1500;
//fichier où on écrit
ofstream fichier;

void ajouterFichier(const std::string& time){
  fichier << nbImages << " " << time << " " << std::left << std::setw(12) << std::setfill('0') << exposureTime << endl;
}

void saveImage(const cv::Mat& image){
  stringstream numero;
  numero << nbImages;
  imwrite(("ressourcesCalib/images/image_" + numero.str() + ".jpg"),image);
}

//boucle sur /camera
void imageCallback(const sensor_msgs::ImageConstPtr& msg){
    //On traduit l'image ros en image exploitable par opencv
    cv::Mat image = cv_bridge::toCvCopy(msg,"rgb8")->image;
    cv::cvtColor(image, image, CV_RGB2BGR);
    cv::imshow("retour",image);
    //On récupère le timestamp de l'image actuel + on incrément le nombre d'images
    std::stringstream timesTamp;
    timesTamp << msg->header.stamp.sec << "." << std::setw(9) << std::setfill('0') << msg->header.stamp.nsec << "0";

    //On ajoute au fichier times.txt + on save l'image
    ajouterFichier(timesTamp.str());
    saveImage(image);

    //Pour infos
    nbImages++;
    ROS_INFO("%d : Timestamp : %s",nbImages, timesTamp.str().c_str());
    //On wait
    cv::waitKey(1);
}

int main(int argc,char **argv){
  //Init ros
  ros::init(argc,argv,"generation_text_file");
  ros::NodeHandle n;

  //On supprime le dossier où se trouve les anciennes images
  if(-1 == system("rm -rf ressourcesCalib/images")) ROS_ERROR("impossible de supprimer le dossier d'images!\n");
	if(-1 == system("mkdir -p ressourcesCalib/images")) ROS_ERROR("impossible de créer le dossier d'images!\n");

  fichier.open("ressourcesCalib/times.txt");

  //Subscribers
  ros::Subscriber sub = n.subscribe("/camera/rgb/image_raw",1,imageCallback);
  //création de la fenetre pour opencv
  //cv::namedWindow("retour");

  while (nbImages < maxImage && ros::ok())
    ros::spinOnce();
  ROS_INFO("Génération terminée (les %d photos ont été générés avec leur fichier times.txt)",maxImage);
  ROS_INFO("Generation du pcalib.txt de base qui sera ajusté!");
  fichier.close();
  fichier.open("ressourcesCalib/pcalib.txt");
  for(int i = 0 ;i <= 255;i++){
    fichier << i << " ";
  }
  fichier.close();
  ROS_INFO("Génération terminée!");
  return 0;
}
