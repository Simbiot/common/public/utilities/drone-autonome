cmake_minimum_required(VERSION 2.8.3)
project(camera_run)

find_package(catkin REQUIRED COMPONENTS
  cv_bridge
  roscpp
)

find_package(OpenCV)

catkin_package()

# Si installation differente de v4l2-capture (san utiliser sudo make install) :
# Decommenter les deux prochaines lignes et commenter les deux lignes d'après
# Adapter le chemin pour qu'il corresponde à votre installation de drone-autonome

#set(LIB_PATH /home/pilote/drone-autonome/ressources/librairie-v4l2-capture)
#set(INCLUDE_PATH ${LIB_PATH})

set(LIB_PATH /usr/local/lib)
set(INCLUDE_PATH /usr/local/include/v4l2_capture)

include_directories(
  ${catkin_INCLUDE_DIRS}
  ${INCLUDE_PATH}
)

set(v4l2_capture_lib ${LIB_PATH}/libv4l2_capture.a)

add_executable(bottom src/bottom.cpp)
target_link_libraries(bottom ${catkin_LIBRARIES} ${v4l2_capture_lib})

add_executable(front src/front.cpp)
target_link_libraries(front ${catkin_LIBRARIES} ${v4l2_capture_lib})

add_executable(convert_ir src/convert_ir.cpp)
target_link_libraries(convert_ir ${catkin_LIBRARIES})
